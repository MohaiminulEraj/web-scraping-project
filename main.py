#####################################
# Name: S. M. Mohaiminul Islam Eraj #
# ID: 1721499                       #
# Semester: Autumn                  #
#####################################

import requests
from bs4 import BeautifulSoup
import os
import time
import json
ts = time.time()

# URL = input('Enter URL: ')
URL = 'https://www.ebay.com/b/Laptops-Netbooks/175672/bn_1648276?rt=nc&_pgn='
baseURL = URL.split('/')[2]
folder = "Files of " + baseURL
res = requests.get(URL)
soup = BeautifulSoup(res.text, 'html.parser')

try:
    os.mkdir(os.path.join(os.getcwd(), folder))
except:
    pass
os.chdir(os.path.join(os.getcwd(), folder))


def scrape_form_ebay():
    try:
        i = 0
        for x in range(1, 38):
            res = requests.get(URL+str(x))
            soup = BeautifulSoup(res.text, 'html.parser')
            items = {}
            image_download(i)
            # items['product_description'] = []
            for link in soup.find_all('a', class_='s-item__link'):
                # print(link['href'])
                i += 1
                # items[f'product_{i}_description'] = []
                items[f'product_{i}_name'] = link.text.strip()
                for price in soup.find_all('span', class_='s-item__price'):
                    items[f'product_{i}_price'] = price.text.strip()
                    pass
                items[f'product_{i}_link'] = link['href']

                # item_soup = BeautifulSoup(requests.get(link['href']).text, 'html.parser')
            try:
                with open(f"{baseURL}_{ts}_1721499.txt", "a") as f:
                    f.seek(0)
                    item_obj = json.dumps(items)
                    f.write(item_obj)
                    print('[-] Successfully saved form data in text file!')
            except:
                print('[x] Failed to store form data in text file!')
                continue
            time.sleep(3)
    except:
        print('[x] Failed to scrape data from ebay!')
        return


def tabular_data():
    try:
        rows_of_table = "["
        table_data = soup.find('table')
        for header in table_data.find_all('th'):
            title = header.text.strip()
            rows_of_table += f"{str(title)}, "
        rows_of_table += "]\n"

        if(len(rows_of_table) == 3):
            rows_of_table = ""
            for row in table_data.find_all('tr'):
                data = row.find_all('td')
                row_data = [td.text.strip() for td in data]
                rows_of_table += str(row_data)
                rows_of_table += "\n"
        else:
            for row in table_data.find_all('tr')[1:]:
                data = row.find_all('td')
                row_data = [td.text.strip() for td in data]
                rows_of_table += str(row_data)
                rows_of_table += "\n"

        with open(f"{baseURL}_{ts}_1721499.txt", "a") as f:
            f.seek(0)
            f.write(rows_of_table)
    except:
        pass


def image_download(i):
    try:
        images = soup.find_all('img', class_='s-item__image-img')
        j = i
        for image in images:
            j += 1
            link = image['src']
            l = link[0:4]
            b = baseURL[6:10]
            if (l != 'http' and b == l):
                link = "https:" + link
            elif(l != 'http'):
                link = baseURL + link

            with open(str(j) + '.jpg', 'wb') as f:
                img = requests.get(link)
                f.write(img.content)
                print('writing: ', j)
    except:
        print('[x] Failed to download image file!')
        pass


def file_download():
    try:
        files = soup.find_all('a')
        i = 0
        for file in files:
            link = file['href']
            l = link[0:4]
            b = baseURL[6:10]
            if (l != 'http' and b == l):
                link = "https:" + link
            elif(l != 'http'):
                link = baseURL + link
            ext = ''
            if "." in link[len(link)-5:len(link)]:
                i += 1
                ext = link[len(link)-5:len(link)].split(".")[1]
                with open(str(i) + '.' + ext, 'wb') as f:
                    myFile = requests.get(link)
                    f.write(myFile.content)
                    print('writing: ', i)
    except:
        pass


scrape_form_ebay()
tabular_data()
file_download()
